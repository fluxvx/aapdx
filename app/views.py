
from django.utils import timezone

from django.shortcuts import render, reverse, redirect

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseForbidden

from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User, Group, Permission
# from django.contrib.auth.decorators import login_required
from .models import Event, QueuedFacebookEvent, Category, Organization
from django.core.paginator import Paginator
import datetime
import pytz



def index(request):
    return render(request, 'app/index.html', {'categories': Category.objects.order_by('name')})



def login_page(request):

    # if request.user.is_authenticated:
    #     return HttpResponseRedirect(reverse(''))

    message = ''
    if 'logout' in request.GET.keys():
        message = 'You have successfully logged out.'
    if 'redirect' in request.GET.keys():
        message = 'You must log in to view this page.'

    next = request.GET.get('next', '')

    return render(request, 'app/login.html', {'message': message, 'next': next})



def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        if 'next' in request.GET.keys():
            return HttpResponseRedirect(request.GET['next'])
        else:
            return HttpResponseRedirect(reverse('app:index'))
    else:
        return HttpResponseRedirect(reverse('app:registration_login'))


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('app:registration_login')+'?logout')
    #return redirect(reverse('userapp:registration_login')+'?logout=true')



def parse_date(s):
    r = datetime.datetime.strptime(s, '%Y-%m-%d')
    r = pytz.timezone('US/Pacific').localize(r)
    return r



def get_orgs(request):
    page = request.GET.get('page', 1)

    category_id = request.GET.get('category_id', None)
    if category_id == '' or category_id == '0':
        category_id = None

    orgs = Organization.objects.order_by('name')
    if category_id is not None:
        orgs = orgs.filter(category_id=category_id)

    orgs_per_page = 20
    paginator = Paginator(orgs, orgs_per_page)
    orgs_on_page = paginator.page(page)
    data = {'orgs': [], 'categories': [], 'pages': paginator.num_pages}
    for org in orgs_on_page:
        data['orgs'].append(org.toDictionary())

    categories = []
    for org in orgs:
        categories.append(org.category.toDictionary())
    categories = list({category['id']:category for category in categories}.values())
    for category in categories:
        data['categories'].append(category)

    return JsonResponse(data)


def get_events(request):

    page = request.GET.get('page', 1)

    start_date = request.GET.get('start_date', None)
    if start_date == '':
        start_date = None
    if start_date is not None:
        start_date = parse_date(start_date)
        #start_date.replace(hour=0, minute=0, second=0)

    end_date = request.GET.get('end_date', None)
    if end_date == '':
        end_date = None
    if end_date is not None:
        end_date = parse_date(end_date)
        end_date += datetime.timedelta(days=1)
        #end_date.replace(hour=23, minute=59, second=59)

    category_id = request.GET.get('category_id', None)
    if category_id == '' or category_id == '0':
        category_id = None

    events = Event.objects.filter(approved=True).order_by('start_datetime')
    if start_date is not None:
        events = events.filter(end_datetime__gte=start_date)
    if end_date is not None:
        events = events.filter(start_datetime__lte=end_date)
    if category_id is not None:
        events = events.filter(category_id=category_id)

    events_per_page = 20
    paginator = Paginator(events, events_per_page)
    events_on_page = paginator.page(page)
    data = {'events': [], 'categories': [], 'pages': paginator.num_pages}
    for event in events_on_page:
        data['events'].append(event.toDictionary())


    categories = []
    for event in events:
        if event.category:
            categories.append(event.category.toDictionary())
    categories = list({category['id']:category for category in categories}.values())
    for category in categories:
        data['categories'].append(category)

    return JsonResponse(data)




def submit_facebook_event(request):
    if request.method == 'GET':
        return render(request, 'app/submit_facebook_event.html', {})



    recaptcha_response = request.POST.get('g-recaptcha-response')
    r = requests.post('https://www.google.com/recaptcha/api/siteverify', data={
        'secret': local_settings.recaptcha_secret_key,
        'response': recaptcha_response
    })

    result = r.json()
    if not result['success']:
        return render(request, 'app/submit_facebook_event.html', {'message_error':'Invalid reCAPTCHA!'})


    url = request.POST['facebook_url']

    if QueuedFacebookEvent.objects.filter(url=url).exists() \
        or Event.objects.filter(link=url).exists():
        return render(request, 'app/submit_facebook_event.html', {'message_success': 'That event has already been submitted!'})


    qfe = QueuedFacebookEvent(url=url, completed=False)
    qfe.save()

    return render(request, 'app/submit_facebook_event.html', {'message_success': 'Event Submitted! Thank You!!'})



from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from pyvirtualdisplay import Display
from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import sys
from . import local_settings


def load_pending_facebook_event(pending_event, driver):
    url = pending_event.url
    driver.get(url)

    soup = BeautifulSoup(driver.page_source, 'html.parser')

    # title
    title = soup.html.head.title.string

    if title == 'Facebook':
        print('event deleted or improperly loaded')
        return

    # description
    description = soup.find('div', {'data-testid': 'event-permalink-details'})
    if description is None:
        description = ''
    else:
        description = description.find('span').decode_contents()

    # start datetime
    # this might be done better, there's an li higher up with id=event_time_info
    # this seems to be the only element with a content attribute
    datetimes = soup.find('div', {'class': ['_2ycp', '_5xhk']})
    datetimes = datetimes['content']
    start_datetime = datetimes[:19]
    end_datetime = datetimes[29:48]
    format = "%Y-%m-%dT%H:%M:%S"
    start_datetime = datetime.datetime.strptime(start_datetime, format)
    if end_datetime == '':
        end_datetime = None
    else:
        end_datetime = datetime.datetime.strptime(end_datetime, format)

    location_patterns = (('a', {'id': 'u_0_n'}),
                         ('a', {'id': 'u_0_18'}),
                         ('a', {'id': 'u_0_19'}),
                         ('a', {'id': 'u_0_1i'}),
                         ('a', {'id': 'u_0_w'}),
                         ('a', {'id': 'u_0_r'}),
                         ('span', {'class': '_5xhk'}))

    location = ''
    for i, pattern in enumerate(location_patterns):
        location_node = soup.find(pattern[0], pattern[1])
        if location_node is not None:
            location = location_node.text
            location_detail = location_node.find_next_sibling('div', {'class': '_5xhp fsm fwn fcg'})
            if location_detail is not None:
                location += ' ' + location_detail.text
            break

    hosts = soup.find('div', {'data-testid': 'event_permalink_feature_line'})
    hosts = hosts['content'].replace(' & ', ', ')

    event = Event(title=title,
                  description=description,
                  start_datetime=start_datetime,
                  end_datetime=end_datetime,
                  location=location,
                  approved=False,
                  hosts=hosts,
                  link=url)
    event.save()
    pending_event.delete()

def load_pending_facebook_events():
    pending_events = QueuedFacebookEvent.objects.all()

    with Display():
        # binary = FirefoxBinary('/usr/local/bin/firefox')
        # driver = webdriver.Firefox(firefox_binary=binary, executable_path='/home/flux2341/geckodriver')
        driver = None
        try:
            driver = webdriver.Firefox()
            driver.implicitly_wait(30)
            for pending_event in pending_events:
                try:
                    load_pending_facebook_event(pending_event, driver)
                except Exception as e:
                    print(e, file=sys.stderr)
        except Exception as e:
            print(e, file=sys.stderr)
        finally:
            if driver is not None:
                driver.quit()




def pending_facebook_events(request):
    if not request.user.is_superuser:
        return HttpResponseForbidden()
    message = ''
    if request.method == 'POST':
        if request.GET['action'] == 'add':
            url = request.POST['facebook_url']
            if QueuedFacebookEvent.objects.filter(url=url).exists() \
                    or Event.objects.filter(link=url).exists():
                message = 'That event has already been submitted!'
            else:
                qfe = QueuedFacebookEvent(url=url)
                qfe.save()
        elif request.GET['action'] == 'load':
            load_pending_facebook_events()
            message = 'events loaded successfully'
    pending_events = QueuedFacebookEvent.objects.all()
    return render(request, 'app/pending_facebook_events.html', {'pending_events': pending_events, 'message': message})


def get_week_dates(index):
    today = datetime.datetime.now().date()
    current_week_start = today - datetime.timedelta(days=today.weekday())
    week_start = current_week_start + datetime.timedelta(days=7*index)
    week_end = current_week_start + datetime.timedelta(days=7*index+6)
    return week_start, week_end


def event_post(request):
    weeks = []
    for i in range(-2, 3):
        week_start, week_end = get_week_dates(i)
        weeks.append({
            'index': i,
            'name': week_start.strftime('%m/%d') + ' - ' + week_end.strftime('%m/%d')
        })
    week_index = 0 if request.method != 'POST' else int(request.POST['week_index'])
    week_start, week_end = get_week_dates(week_index)
    week_end += datetime.timedelta(days=1)
    events = Event.objects.filter(approved=True).order_by('start_datetime')
    events = events.filter(end_datetime__gte=week_start)
    events = events.filter(start_datetime__lte=week_end)
    return render(request, 'app/event_post.html', {'weeks': weeks, 'events': events, 'week_index': week_index})
