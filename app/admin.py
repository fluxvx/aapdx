from django.contrib import admin

from .models import Event, QueuedFacebookEvent, Organization, Category

admin.site.register(Event)
admin.site.register(QueuedFacebookEvent)
admin.site.register(Organization)
admin.site.register(Category)
