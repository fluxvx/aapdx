
from django.core.management.base import BaseCommand
from app.models import Event
from django.contrib.auth.models import User
from django.utils import timezone
import datetime

class Command(BaseCommand):

    def handle(self, *args, **options):
        now = timezone.now()
        for i in range(100):
            title = 'Event ' + str(i)
            description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            start_datetime = now + datetime.timedelta(days=i)
            end_datetime = start_datetime
            location = '732 Virginia Lane, Uniondale, NY 11553'
            hosts = 'Lorem Ipsum'
            public = True
            link = 'http://www.google.com/'
            approved = True
            event = Event(title=title,
                            description=description,
                            start_datetime=start_datetime,
                            end_datetime=end_datetime,
                            location=location,
                            hosts=hosts,
                            public=public,
                            link=link,
                            approved=approved)
            event.save()
