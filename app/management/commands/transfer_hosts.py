
from django.core.management.base import BaseCommand
from app.models import Event

class Command(BaseCommand):

    def handle(self, *args, **options):
        for event in Event.objects.all():
            organizations = event.organizations.all()
            event.hosts = ', '.join([organization.name for organization in organizations])
            event.save()

